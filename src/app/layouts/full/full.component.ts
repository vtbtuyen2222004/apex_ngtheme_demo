import { Component, OnInit, Inject,
  Renderer2,
  AfterViewInit,
  OnDestroy,
  ChangeDetectionStrategy,
 } from '@angular/core';
 import * as feather from 'feather-icons';

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit, AfterViewInit {
  constructor(){ }
  ngOnInit(): void {
      
  }
  ngAfterViewInit(): void {
      feather.replace();
  }

}
