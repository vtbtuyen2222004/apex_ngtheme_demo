import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import * as feather from 'feather-icons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit  { 
  loginForm = new FormGroup({
    username: new FormControl('guest@apex.com', [Validators.required]),
    password: new FormControl('........', [Validators.required])
  });
  
  submitted = false;

  constructor (private router: Router) { }

  ngOnInit(): void { }
  ngAfterViewInit(): void {
    feather.replace();
}

  public onSubmit() {
    console.log('Information Login: ')
    console.log('username = ' + this.loginForm.controls.username.value);
    console.log('password = ' + this.loginForm.controls.password.value);  
  }


}
